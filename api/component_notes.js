let hx = require('hbuilderx');

function getLine(editor, lineNum) {
	const linePromise = editor.document.lineAt(lineNum);
	return linePromise;
}

function componentNotes() {
	let editorPromise = hx.window.getActiveTextEditor();
	editorPromise.then((editor) => {
		if(editor.document.languageId !== "vue") {
			return;
		}
		const lineCount = editor.document.lineCount;
		let promises = [];
		let textes = [];
		let insertPoint = 0;
		const regScript = /(?<=<script\b[^>]*>)[\s\S]*(?=<\/script>)/g;
		for(let i=0; i < lineCount; i++) {
			promises.push(getLine(editor, i));
		}
		let template = [
			"\n",
			"/*",
			"* 组件名称 组件文字介绍",
			"* @description 组件描述",
			"*/"
		];
		Promise.all(promises)
			.then(fileContent => {
				let flag = false;
				let leftLen = 0;
				let rightLen = 0;
				fileContent.forEach(line => {
					if(line.text === "<script>") {
						insertPoint = line.start + 8;
					}
					if(/props/.test(line.text)) {
						flag = true;
					}
					if(flag) {
					 const text = line.text;
					 const leftRegLen = text.match(/({)/g) && text.match(/({)/g).length;
					 const rightRegLen = text.match(/(})/g) && text.match(/(})/g).length;
					 if(leftRegLen) {
						leftLen += leftRegLen;
					 }
					 if(rightRegLen) {
					 	rightLen += rightRegLen;
					 }
					 if(rightLen >= leftLen) {
						 flag = false;
					 }
					 textes.push(text.replace(/\/\/[\u4e00-\u9fa5|\S|\s|\w]+/g, ""));
					}
				})
				const textStr = textes.join("\n").replace(/\s+/g, "");
				const resStr = textStr.replace(/(props:|,?$)/g, "");
				if(!resStr){
					return;
				}
				let props = {};
				const propList = resStr.match(/[a-zA-Z]+:\{.*?\}/g);
				propList.forEach(propItem => {
					const key = propItem.split(":")[0];
					const val = propItem.match(/type:(\w+(\|\w+)*)/)[1];
					props[key] = val;
				})
				let point = 4;
				Object.keys(props).forEach(key => {
					console.log(props[key])
					const propertyType = props[key];
					template.splice(point,0,`* @property {${propertyType}} ${key} 属性说明`);
					point++;
				})
				editor.edit(editBuilder => {
					editBuilder.insert(insertPoint, template.join("\n")+"\n");
				});
			});
	});
}

module.exports = {
	componentNotes
};
