let hx = require('hbuilderx');
var dayjs = require('dayjs');

function insertNotes() {
	let editorPromise = hx.window.getActiveTextEditor();
	editorPromise.then((editor) => {
		let document = editor.document;
		let selection = editor.selection;
		const extname = document.languageId;
		// Get the word within the selection
		selection.start = 0;
		selection.end = 0;
		let template = [
			"* @Author: your name",
			"* @Date: ${day}",
			"* @LastEditTime: ${day}",
			"* @LastEditors: Please set LastEditors",
			"* @Description: In User Settings Edit"
		];
		if (extname === "vue" || extname === "html") {
			template.unshift("<!--")
			template.push("-->")
		} else {
			template.unshift("/*")
			template.push("*/")
		};
		let values = template.join("\n").replace(/\${day}/g, dayjs().format('YYYY-MM-DD HH:mm:ss'));
		editor.edit(editBuilder => {
			editBuilder.insert(0, values + "\n");
		});
	});
}

module.exports = {
	insertNotes
};
