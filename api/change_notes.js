let hx = require('hbuilderx');
var dayjs = require('dayjs');

function updateNotes() {
	let editorPromise = hx.window.getActiveTextEditor();
	editorPromise.then((editor) => {
		let linePromise = editor.document.lineAt(3);
		linePromise.then((line) => {
			editor.edit(editBuilder => {
				editBuilder.replace({
					start: line.start,
					end: line.end
				}, `* @LastEditTime: ${dayjs().format('YYYY-MM-DD HH:mm:ss')}`);
			});
		});
	});
}

module.exports = {
	updateNotes
};
